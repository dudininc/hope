import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'block-i',
  templateUrl: './block-i.component.html',
  styleUrls: ['./block-i.component.scss']
})
export class BlockIComponent implements OnInit {
  public count: any;

  @Input() public ui: any;

  constructor() {
    this.count = { list: 0 };
  }
  ngOnInit() { }

  ngAfterContentInit(): void {
    if (this.ui) this.getCount();
  }

  private getCount() {
    let arr = this.ui.count.split(".");
    this.count.list = arr[arr.length - 1]
  }

}
