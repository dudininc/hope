import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'block-j',
  templateUrl: './block-j.component.html',
  styleUrls: ['./block-j.component.scss']
})
export class BlockJComponent implements OnInit {
  public count: any;

  @Input() public ui: any;

  constructor() {
    this.count = { context: 0 };
  }
  ngOnInit() { }

  ngAfterContentInit(): void {
    if (this.ui) this.getCount();
  }

  private getCount() {
    let arr = this.ui.count.split(".");
    this.count.context = arr[0];
  }

}
