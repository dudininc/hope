import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'block-f',
  templateUrl: './block-f.component.html',
  styleUrls: ['./block-f.component.scss']
})
export class BlockFComponent implements OnInit {

  public urls: string[] = ["individual-therapy", "pair-therapy", "family-therapy"];

  constructor(private Router: Router) { }
  ngOnInit() { }

  public toPage(url: string): void {
    if (url != null) this.Router.navigate([url]);
  }

}
