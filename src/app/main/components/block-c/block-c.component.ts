import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'block-c',
  templateUrl: './block-c.component.html',
  styleUrls: ['./block-c.component.scss']
})
export class BlockCComponent implements OnInit {

  @Output() onScroll = new EventEmitter();

  constructor() { }
  ngOnInit() { }

  public toContact(){
    this.onScroll.emit();
  }
}
