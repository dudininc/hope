import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'block-d',
  templateUrl: './block-d.component.html',
  styleUrls: ['./block-d.component.scss']
})
export class BlockDComponent implements OnInit {

  @Input() public hrefs: any[];

  constructor(private Router: Router) { }
  ngOnInit() { }

  public toPage(url: string): void {
    if (url != null) this.Router.navigate([url]);
  }
}