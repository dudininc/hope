import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'block-0j',
  templateUrl: './block-0j.component.html',
  styleUrls: ['./block-0j.component.scss']
})
export class Block0JComponent implements OnInit {
  public count: any;

  @Input() public ui: any;

  constructor() {
    this.count = { list: 0 };
  }
  ngOnInit() { }

  ngAfterContentInit(): void {
    if (this.ui) this.getCount();
  }

  private getCount() {
    let arr = this.ui.count.split(".");
    this.count.list = arr[arr.length - 1]
  }
}
