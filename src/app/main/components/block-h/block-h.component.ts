import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'block-h',
  templateUrl: './block-h.component.html',
  styleUrls: ['./block-h.component.scss']
})
export class BlockHComponent implements OnInit {
  @Input() public ui: any;
  @Output() onScroll = new EventEmitter();

  constructor() { }
  ngOnInit() { }

  public isParam(param): boolean {
    return Boolean(this.ui[param]);
  }

  public toContact(){
    this.onScroll.emit();
  }

}
