import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { DemoNumberPipe } from '../_services/demo-number.pipe';
import { BlockCComponent } from './components/block-c/block-c.component';
import { BlockDComponent } from './components/block-d/block-d.component';
import { BlockEComponent } from './components/block-e/block-e.component';
import { BlockFComponent } from './components/block-f/block-f.component';
import { BlockGComponent } from './components/block-g/block-g.component';
import { BlockHComponent } from './components/block-h/block-h.component';
import { BlockIComponent } from './components/block-i/block-i.component';
import { BlockJComponent } from './components/block-j/block-j.component';
import { Block0JComponent } from './components/block-0j/block-0j.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    AngularFontAwesomeModule
  ],
  declarations: [
    DemoNumberPipe, BlockCComponent, BlockDComponent, BlockEComponent,
    BlockFComponent, BlockGComponent, BlockHComponent, BlockIComponent, 
    BlockJComponent, Block0JComponent
  ],
  exports: [
    DemoNumberPipe, BlockCComponent, BlockDComponent, BlockEComponent,
    BlockFComponent, BlockGComponent, BlockHComponent, BlockIComponent,
    BlockJComponent, Block0JComponent
  ]
})
export class MainModule { }
