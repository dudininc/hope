import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgModule, APP_INITIALIZER, Injector } from '@angular/core';
import { ScrollToModule } from '../libs/ng2-scroll-to-el';

import { DashboardModule } from './dashboard/dashboard.module';
import { HeaderModule } from './header/header.module';
import { MainModule } from './main/main.module';
import { AppComponent } from './app.component';

import { TranslateInitializerFactory } from './_services/translate-initializer.service';
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/');
}

const router: Route[] = [
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(router),
    ScrollToModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    DashboardModule,
    HeaderModule,
    MainModule
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: TranslateInitializerFactory, deps: [TranslateService, Injector], multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
