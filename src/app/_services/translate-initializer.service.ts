import { Injector } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LOCATION_INITIALIZED } from '@angular/common';

export function TranslateInitializerFactory(translate: TranslateService, injector: Injector) {
    return () => new Promise<any>((resolve: any) => {
        const locationInitialized = injector.get(LOCATION_INITIALIZED, Promise.resolve(null));
        locationInitialized.then(() => {
            let langToSet = window.location.pathname.slice(1);
            langToSet.match(/en|ru/) ? null : langToSet = 'ru';                 
            translate.setDefaultLang(langToSet);
            translate.use(langToSet).subscribe(() => { }, err => { }, () => {
                resolve(null);
            });
        });
    });
}