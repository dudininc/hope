import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MenuCComponent } from './components/menu-c/menu-c.component';
import { BgfaceCComponent } from './components/bgface-c/bgface-c.component';
import { BgfaceDComponent } from './components/bgface-d/bgface-d.component';
import { MenuBurgerComponent } from './components/menu-burger/menu-burger.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    AngularFontAwesomeModule
  ],
  declarations: [MenuCComponent, BgfaceCComponent, BgfaceDComponent, MenuBurgerComponent],
  exports: [MenuCComponent, BgfaceCComponent, BgfaceDComponent]
})
export class HeaderModule { }
