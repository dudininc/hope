import { Component, OnInit, HostListener, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'menu-c',
  templateUrl: './menu-c.component.html',
  styleUrls: ['./menu-c.component.scss']
})
export class MenuCComponent implements OnInit {
  public isBtnTop: boolean;
  public isMenuShow: boolean;

  @Input('menu') public main_menu: any[];
  @Output() toTop = new EventEmitter();
  @Output() toContact = new EventEmitter();

  @HostListener('window:scroll', ['$event'])
  private checkScroll() {
    this.isBtnTop = Boolean(window.pageYOffset > 50);
  }

  @HostListener('window:resize', ['$event'])
  private checkResize() {
    this.isMenuShow = false;
  }

  constructor(private Router: Router) { }
  ngOnInit() { }

  public toPage(item: any): void {
    if (item.id === 0) {
      this.toScroll();
    } else {
      if (item.active == null) {
        if (item.url !== null) {
          this.checkResize();
          this.Router.navigate([item.url]);
        }
      } else {
        item.active = !item.active;
      }
    }
  }

  public toScroll(isTop = false): void {
    isTop ? this.toTop.emit() : this.toContact.emit();
  }

  public qqq() {
    console.log(1);
  }

}
