import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'bgface-d',
  templateUrl: './bgface-d.component.html',
  styleUrls: ['./bgface-d.component.scss']
})
export class BgfaceDComponent implements OnInit {
  
  @Input() public ui: any;

  constructor() { }
  ngOnInit() { }

}
