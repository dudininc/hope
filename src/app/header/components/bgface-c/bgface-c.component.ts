import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'bgface-c',
  templateUrl: './bgface-c.component.html',
  styleUrls: ['./bgface-c.component.scss']
})
export class BgfaceCComponent implements OnInit {

  @Output() onScroll = new EventEmitter();

  constructor() { }
  ngOnInit() { }

  public toContact(){
    this.onScroll.emit();
  }

}
