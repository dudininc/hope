import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HeaderModule } from '../header/header.module';
import { MainModule } from '../main/main.module';
import { FooterModule } from '../footer/footer.module';
import { SpaHeaderComponent } from './components/spa-header/spa-header.component';
import { SpaMainComponent } from './components/spa-main/spa-main.component';
import { SpaFooterComponent } from './components/spa-footer/spa-footer.component';
import { AppDashboardComponent } from './components/app-dashboard/app-dashboard.component';
import { AppFaqComponent } from './components/app-faq/app-faq.component';
import { AppTherapyComponent } from './components/app-therapy/app-therapy.component';
import { AppServiceComponent } from './components/app-service/app-service.component';

const router: Route[] = [
  { path: '', component: AppDashboardComponent },
  { path: 'individual-therapy', component: AppTherapyComponent },
  { path: 'pair-therapy', component: AppTherapyComponent },
  { path: 'family-therapy', component: AppTherapyComponent },
  { path: 'depression', component: AppServiceComponent },
  { path: 'personal-growth', component: AppServiceComponent },
  { path: 'anxiety', component: AppServiceComponent },
  { path: 'loss-grief', component: AppServiceComponent },
  { path: 'postpartum-depression', component: AppServiceComponent },
  { path: 'relationships', component: AppServiceComponent },
  { path: 'life-transition', component: AppServiceComponent },
  { path: 'faqs', component: AppFaqComponent }
];

@NgModule({
  imports: [
    CommonModule,
    HeaderModule,
    MainModule,
    FooterModule,
    RouterModule.forChild(router)
  ],
  declarations: [
    AppDashboardComponent,
    SpaHeaderComponent,
    SpaMainComponent,
    SpaFooterComponent,
    AppFaqComponent,
    AppTherapyComponent,
    AppServiceComponent
  ],
  exports: [SpaFooterComponent]
})
export class DashboardModule { }
