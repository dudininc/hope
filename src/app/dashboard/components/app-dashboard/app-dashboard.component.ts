import { Component, OnInit } from '@angular/core';
import { ScrollToService } from 'src/libs/ng2-scroll-to-el';

@Component({
  selector: 'app-dashboard',
  templateUrl: './app-dashboard.component.html',
  styleUrls: ['./app-dashboard.component.scss']
})
export class AppDashboardComponent implements OnInit {

  public ui: any = {
    title: "CONTENT.TITLE",
    text: "CONTENT.TEXT",
    context: "CONTENT.CONTEXT",
    btn: true
  };

  constructor(private ScrollToServicey: ScrollToService) { }
  ngOnInit() { }

  public toScroll(target): void{
    this.ScrollToServicey.scrollTo(target);
  }

}
