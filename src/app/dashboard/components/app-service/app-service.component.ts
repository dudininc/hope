import { Component, OnInit } from '@angular/core';
import { ScrollToService } from 'src/libs/ng2-scroll-to-el';
import { Router } from '@angular/router';

@Component({
  selector: 'app-service',
  templateUrl: './app-service.component.html',
  styleUrls: ['./app-service.component.scss']
})
export class AppServiceComponent implements OnInit {

  public url: string;
  public bg: any;

  constructor(
    private Router: Router,
    private ScrollToServicey: ScrollToService
  ) {
    this.url = this.Router.url.slice(1);
    this.bg = {
      img: `assets/images/${this.url}.jpg`,
      title: `SERVICE.${this.url}.TITLE`,
      context: `SERVICE.${this.url}.CONTEXT`
    };
  }
  ngOnInit() { }

  public toScroll(target): void {
    this.ScrollToServicey.scrollTo(target);
  }

}
