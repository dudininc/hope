import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ScrollToService } from 'src/libs/ng2-scroll-to-el';

@Component({
  selector: 'app-therapy',
  templateUrl: './app-therapy.component.html',
  styleUrls: ['./app-therapy.component.scss']
})
export class AppTherapyComponent implements OnInit {

  public url: string;
  public ui: any;
  public content: any;
  public general: any[];
  public data: any;

  constructor(
    private Router: Router,
    private ScrollToServicey: ScrollToService
  ) {
    const hardcode_context = {
      content: {
        "individual-therapy": "2.13",
        "pair-therapy": "2.9",
        "family-therapy": "1.7"
      },

      general: {
        "individual-therapy": ["1.0", "1.0"],
        "pair-therapy": ["2.0", "1.0"],
        "family-therapy": ["2.0", "0.0"]
      },

      data:{
        "individual-therapy": "0.7",
        "pair-therapy": "0.0",
        "family-therapy": "0.3"
      }

    };

    this.url = this.Router.url.slice(1);

    this.ui = {
      img: `assets/images/${this.url}.jpg`,
      title: `THERAPY.${this.url}.TITLE`,
      context: `THERAPY.${this.url}.CONTEXT`
    };

    this.content = {
      first: `THERAPY.${this.url}.DATA.FIRST`,
      second: `THERAPY.${this.url}.DATA.SECOND`,
      bold: (this.url !== "pair-therapy") ? `THERAPY.${this.url}.DATA.BOLD` : false,
      context: `THERAPY.${this.url}.DATA.CONTEXT`,
      list: `THERAPY.${this.url}.DATA.LIST`,
      count: hardcode_context.content[this.url]
    };    

    this.general = [
      {
        first: `THERAPY.${this.url}.DATA2.TITLE`,
        bold: this.url !== "individual-therapy" ? `THERAPY.${this.url}.DATA2.BOLD` : false,
        context: `THERAPY.${this.url}.DATA2.CONTEXT`,
        count: hardcode_context.general[this.url][0]
      },
      {
        isNotTitle: (this.url === "pair-therapy" || this.url === "family-therapy"),
        first: `THERAPY.${this.url}.DATA3.TITLE`,
        bold: this.url !== "individual-therapy" ? `THERAPY.${this.url}.DATA3.BOLD` : false,
        context: `THERAPY.${this.url}.DATA3.CONTEXT`,
        count: hardcode_context.general[this.url][1]
      }
    ];

    this.data = {
      list: `THERAPY.${this.url}.DATA4.LIST`,
      count: hardcode_context.data[this.url]
    };
  }

  ngOnInit() { }

  public toScroll(target): void{
    this.ScrollToServicey.scrollTo(target);
  }

}
