import { Component, OnInit } from '@angular/core';
import { ScrollToService } from 'src/libs/ng2-scroll-to-el';

export interface IFaqs {
  title: string,
  text: string
  context?: string
}

@Component({
  selector: 'app-faq',
  templateUrl: './app-faq.component.html',
  styleUrls: ['./app-faq.component.scss']
})
export class AppFaqComponent implements OnInit {

  public ui: any = {
    img: "assets/images/faqs.jpg",
    title: "FAQ.TITLE",
    context: "FAQ.CONTEXT"
  };

  constructor(private ScrollToServicey: ScrollToService) { }
  ngOnInit() { }

  public faqs: IFaqs[] = [
    { title: "FAQ.VISIT.TITLE", text: "FAQ.VISIT.TEXT", context: "FAQ.VISIT.CONTEXT" },
    { title: "FAQ.TIME.TITLE", text: "FAQ.TIME.TEXT" },
    { title: "FAQ.MONEY.TITLE", text: "FAQ.MONEY.TEXT" },
    { title: "FAQ.WEEK.TITLE", text: "FAQ.WEEK.TEXT" },
    { title: "FAQ.CHILD.TITLE", text: "FAQ.CHILD.TEXT" },
    { title: "FAQ.CARD.TITLE", text: "FAQ.CARD.TEXT" },
    { title: "FAQ.ANONIM.TITLE", text: "FAQ.ANONIM.TEXT" }
  ];

  public toScroll(target): void{
    this.ScrollToServicey.scrollTo(target);
  }

}
