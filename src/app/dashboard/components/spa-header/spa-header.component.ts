import { Component, AfterContentInit, ContentChild } from '@angular/core';

@Component({
  selector: 'spa-header',
  templateUrl: './spa-header.component.html',
  styleUrls: ['./spa-header.component.scss']
})
export class SpaHeaderComponent implements AfterContentInit {

  constructor() { }
  ngAfterContentInit() { }

}
