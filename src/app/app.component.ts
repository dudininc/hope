import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ScrollToService } from '../libs/ng2-scroll-to-el';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public main_href: any[] = [
    { title: "MENU.THEMES.LEVELUP", url: "personal-growth" },
    { title: "MENU.THEMES.SEX", url: "relationships" },
    { title: "MENU.THEMES.GRUST", url: "depression" },

    { title: "MENU.THEMES.TRANSFORM", url: "life-transition" },

    { title: "MENU.THEMES.TREVOGA", url: "anxiety" },
    { title: "MENU.THEMES.POTERIA", url: "loss-grief" },
    { title: "MENU.THEMES.PREGAT", url: "postpartum-depression" }
  ];
  public menu: any[] = [
    {
      id: 1,
      title: "MENU.HOME",
      url: ""
    },
    {
      id: 2,
      title: "MENU.SERVICE",
      url: null,
      active: false,
      items: [
        { title: "MENU.SERVICE.ONE", url: "individual-therapy" },
        { title: "MENU.SERVICE.PAIR", url: "pair-therapy" },
        { title: "MENU.SERVICE.FAMILY", url: "family-therapy" }
      ]
    },
    {
      id: 3,
      title: "MENU.ABOUT",
      url: null
    },
    {
      id: 4,
      title: "MENU.THEMES",
      url: null,
      active: false,
      items: this.main_href
    },
    {
      id: 5,
      title: "MENU.FAQS",
      url: "faqs"
    },
    {
      id: 0,
      title: "GENERAL.CONTACT",
      url: null
    }
  ];

  constructor(
    private _Router: Router,
    private ScrollToServicey: ScrollToService
  ) { }

  ngOnInit() {
    this._Router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) { return; }
      window.scrollTo(0, 0);
    });
  }

  public toScroll(target) {
    this.ScrollToServicey.scrollTo(target);
  }
}
