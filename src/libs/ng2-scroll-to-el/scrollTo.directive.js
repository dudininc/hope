import { Directive, HostListener, Input } from '@angular/core';
import { ScrollToService } from './scrollTo.service';
var ScrollToDirective = (function () {
    function ScrollToDirective(scrollToService) {
        this.scrollToService = scrollToService;
    }
    ScrollToDirective.prototype.ngOnInit = function () {
        this.scrollDuration = (!this.scrollDuration) ? 500 : this.scrollDuration;
        this.scrollOffset = (!this.scrollOffset) ? 0 : this.scrollOffset;
    };
    ScrollToDirective.prototype.onMouseClick = function () {
        this.scrollToService.scrollTo(this.scrollTo, this.scrollDuration, this.scrollOffset);
    };
    return ScrollToDirective;
}());
export { ScrollToDirective };
ScrollToDirective.decorators = [
    { type: Directive, args: [{
                selector: '[scrollTo]'
            },] },
];
/** @nocollapse */
ScrollToDirective.ctorParameters = function () { return [
    { type: ScrollToService, },
]; };
ScrollToDirective.propDecorators = {
    'scrollTo': [{ type: Input, args: ['scrollTo',] },],
    'scrollDuration': [{ type: Input, args: ['scrollDuration',] },],
    'scrollOffset': [{ type: Input, args: ['scrollOffset',] },],
    'onMouseClick': [{ type: HostListener, args: ['mousedown',] },],
};
//# sourceMappingURL=scrollTo.directive.js.map