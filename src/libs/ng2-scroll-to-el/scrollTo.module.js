import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollToDirective } from './scrollTo.directive';
import { ScrollToService } from './scrollTo.service';
var ScrollToModule = (function () {
    function ScrollToModule() {
    }
    ScrollToModule.forRoot = function () {
        return {
            ngModule: ScrollToModule
        };
    };
    return ScrollToModule;
}());
export { ScrollToModule };
ScrollToModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    ScrollToDirective
                ],
                imports: [
                    CommonModule
                ],
                exports: [
                    ScrollToDirective
                ],
                providers: [
                    ScrollToService
                ]
            },] },
];
/** @nocollapse */
ScrollToModule.ctorParameters = function () { return []; };
//# sourceMappingURL=scrollTo.module.js.map