/**
 * ng2-scroll-to-el - Scroll to element library for agnualr 2+
 * @version v1.2.1
 * @author Marcin Michalik
 * @link https://github.com/MarcinMichalik/ng-scrollTo#readme
 * @license MIT
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@angular/core"), require("rxjs/Subject"), require("@angular/common"));
	else if(typeof define === 'function' && define.amd)
		define(["@angular/core", "rxjs/Subject", "@angular/common"], factory);
	else if(typeof exports === 'object')
		exports["ngScrollTo"] = factory(require("@angular/core"), require("rxjs/Subject"), require("@angular/common"));
	else
		root["ngScrollTo"] = factory(root["ng"]["core"], root["Rx"], root["ng"]["common"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_1__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_6__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var Subject_1 = __webpack_require__(5);
var ScrollToService = (function () {
    function ScrollToService() {
    }
    ScrollToService.prototype.scrollTo = function (element, duration, offset) {
        if (duration === void 0) { duration = 500; }
        if (offset === void 0) { offset = 0; }
        var subject = new Subject_1.Subject();
        if (typeof element === 'string') {
            var el = document.querySelector(element);
            this.scrollToElement(el, duration, offset, subject);
        }
        else if (element instanceof HTMLElement) {
            this.scrollToElement(element, duration, offset, subject);
        }
        else {
            subject.error('I don\'t find element');
        }
        return subject;
    };
    ScrollToService.prototype.scrollToElement = function (el, duration, offset, subject) {
        if (el) {
            var viewportOffset = el.getBoundingClientRect();
            var offsetTop = viewportOffset.top + window.pageYOffset;
            this.doScrolling(offsetTop + offset, duration, subject);
        }
        else {
            subject.error('I don\'t find element');
        }
        return subject;
    };
    ScrollToService.prototype.doScrolling = function (elementY, duration, subject) {
        var startingY = window.pageYOffset;
        var diff = elementY - startingY;
        var start;
        window.requestAnimationFrame(function step(timestamp) {
            start = (!start) ? timestamp : start;
            var time = timestamp - start;
            var percent = Math.min(time / duration, 1);
            window.scrollTo(0, startingY + diff * percent);
            if (time < duration) {
                window.requestAnimationFrame(step);
                subject.next({});
            }
            else {
                subject.complete();
            }
        });
    };
    return ScrollToService;
}());
ScrollToService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ScrollToService);
exports.ScrollToService = ScrollToService;


/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_1__;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var scrollTo_service_1 = __webpack_require__(0);
var ScrollToDirective = (function () {
    function ScrollToDirective(scrollToService) {
        this.scrollToService = scrollToService;
    }
    ScrollToDirective.prototype.ngOnInit = function () {
        this.scrollDuration = (!this.scrollDuration) ? 500 : this.scrollDuration;
        this.scrollOffset = (!this.scrollOffset) ? 0 : this.scrollOffset;
    };
    ScrollToDirective.prototype.onMouseClick = function () {
        this.scrollToService.scrollTo(this.scrollTo, this.scrollDuration, this.scrollOffset);
    };
    return ScrollToDirective;
}());
__decorate([
    core_1.Input('scrollTo'),
    __metadata("design:type", Object)
], ScrollToDirective.prototype, "scrollTo", void 0);
__decorate([
    core_1.Input('scrollDuration'),
    __metadata("design:type", Number)
], ScrollToDirective.prototype, "scrollDuration", void 0);
__decorate([
    core_1.Input('scrollOffset'),
    __metadata("design:type", Number)
], ScrollToDirective.prototype, "scrollOffset", void 0);
__decorate([
    core_1.HostListener('mousedown'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ScrollToDirective.prototype, "onMouseClick", null);
ScrollToDirective = __decorate([
    core_1.Directive({
        selector: '[scrollTo]'
    }),
    __metadata("design:paramtypes", [scrollTo_service_1.ScrollToService])
], ScrollToDirective);
exports.ScrollToDirective = ScrollToDirective;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(1);
var common_1 = __webpack_require__(6);
var scrollTo_directive_1 = __webpack_require__(2);
var scrollTo_service_1 = __webpack_require__(0);
var ScrollToModule = ScrollToModule_1 = (function () {
    function ScrollToModule() {
    }
    ScrollToModule.forRoot = function () {
        return {
            ngModule: ScrollToModule_1
        };
    };
    return ScrollToModule;
}());
ScrollToModule = ScrollToModule_1 = __decorate([
    core_1.NgModule({
        declarations: [
            scrollTo_directive_1.ScrollToDirective
        ],
        imports: [
            common_1.CommonModule
        ],
        exports: [
            scrollTo_directive_1.ScrollToDirective
        ],
        providers: [
            scrollTo_service_1.ScrollToService
        ]
    })
], ScrollToModule);
exports.ScrollToModule = ScrollToModule;
var ScrollToModule_1;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(3));
__export(__webpack_require__(0));
__export(__webpack_require__(2));


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ })
/******/ ]);
});
//# sourceMappingURL=ngScrollTo.umd.js.map